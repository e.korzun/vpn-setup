import axios from "axios";

import {config} from 'dotenv'

config()

const http = axios.create({
  baseURL: process.env.API_ROOT,
  headers: {
    "X-INSTANCE-API-KEY": process.env.INSTANCE_API_KEY,
  },
});

export { http };
export default http;
