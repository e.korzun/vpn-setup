#!/usr/bin/env zx
import "zx/globals";
import fs from "fs";
import http from "./helpers/http.js";
import FormData from "form-data";
require("dotenv").config();

const form = new FormData();

form.append(
  "server_id", "123"
);
form.append(
  "mobileconfig", "xxx"
);
form.append(
  "xxx", await fs.createReadStream("./upload.mjs"), "xxx"
);


const { data } = await http.post(
  `/api/webhooks/server/upload-credentials`,
  form,
  {
    headers: {
      ...form.getHeaders(),
    },
  }
);
console.log(data);
