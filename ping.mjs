#!/usr/bin/env zx
import "zx/globals";
import http from "./helpers/http.js";
import jsoning from "jsoning";
let db = new jsoning("./data/db.json");
require("dotenv").config();


const hostname = (await $`hostname`).toString().trim();

if (
  hostname === "streaming-acatenango"
  || hostname === "mjcomhepyq"
) {
  if(!process.env.DEBUG) {
    console.log("on-start disabled for default server");
    process.exit(0);
  }
}

try {
  setTimeout(async () => {
    await http.post(`/api/webhooks/server/ping`, {
      server_id: await db.get("HOSTNAME"),
      ip: await db.get("IP"),
    });
  }, 1000 * Math.round(Math.random() * 100));
} catch {}
