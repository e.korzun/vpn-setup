#!/usr/bin/env zx
import "zx/globals";
import fs from "fs";
import http from "./helpers/http.js";
import jsoning from "jsoning";
import {config} from 'dotenv'

config()

function generatePassword() {
  var length = 12,
    charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    retVal = "";
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
}

let db = new jsoning("./data/db.json");

// Обновить крон
await $`sudo cat crontab.txt |crontab -`;

try {
  await $`sudo mkdir -p /var/lib/docker/volumes/ikev2-vpn-data/_data`;
} catch {}

const hostname = (await $`hostname`).toString().trim();

if (
  hostname === "streaming-acatenango"
  || hostname === "mjcomhepyq"
) {
  if(!process.env.DEBUG) {
    console.log("on-start disabled for default server");
    process.exit(0);
  }
}


if (!(await db.get("VERSION"))) {
  await db.set(`VERSION`, 1);
}

if (!(await db.get("DOCKER_IPSEC_IMAGE_PULLED"))) {
  await $`docker pull hwdsl2/ipsec-vpn-server`;
  await db.set(`DOCKER_IPSEC_IMAGE_PULLED`, true);
}

if (!(await db.get("IP"))) {
  const ipRaw = (await $`ip r`).toString();
  const ip = ipRaw.match(/src (\d+\.\d+\.\d+\.\d+)/)[1];
  await db.set(`IP`, ip);
  await db.set(`HOSTNAME`, hostname);
}


if (!(await db.get("PSK"))) {
  await db.set(`PSK`, generatePassword());
  await db.set(`DEFAULT_PASSWORD`, generatePassword());
}

if (!(await db.get("USERS"))) {
  await $`docker pull hwdsl2/ipsec-vpn-server`;
  const ip = await db.get("IP");
  const psk = await db.get("PSK");
  await db.set(`USERS`, [
    {
      login: "vpnuser",
      password: await db.get("DEFAULT_PASSWORD"),
      ip,
      psk,
    },
    {
      login: "vpnuser_1",
      password: generatePassword(),
      ip,
      psk,
    },
    {
      login: "vpnuser_2",
      password: generatePassword(),
      ip,
      psk,
    },
    {
      login: "vpnuser_3",
      password: generatePassword(),
      ip,
      psk,
    },
    {
      login: "vpnuser_4",
      password: generatePassword(),
      ip,
      psk,
    },
  ]);
}

if (!(await db.get("ENV_FILE"))) {
  try {
    const [defaultuser, ...users] = await db.get("USERS");
    await fs.writeFileSync(
      "./data/vpn.env",
      `
  #
  VPN_IPSEC_PSK=${await db.get("PSK")}
  VPN_USER=vpnuser
  VPN_CLIENT_NAME=vpnuser
  VPN_PASSWORD=${await db.get("DEFAULT_PASSWORD")}
  VPN_ADDL_USERS=${users.map((i) => i.login).join(" ")}
  VPN_ADDL_PASSWORDS=${users.map((i) => i.password).join(" ")}
  #`
    );
    await db.set("ENV_FILE", true);
  } catch {}
}

if (!(await db.get("DOCKER_IPSEC_INITIALIZED"))) {
  try {
    await $`docker rm -f ipsec-vpn-server`;
    await $`sleep 3`
  } catch {}

  try {
    await $`docker run \
    --name ipsec-vpn-server \
    --restart=always \
    -v "$(pwd)/data/vpn.env:/opt/src/env/vpn.env:ro" \
    -v ikev2-vpn-data:/etc/ipsec.d \
    -v /lib/modules:/lib/modules:ro \
    -p 500:500/udp \
    -p 4500:4500/udp \
    -d --privileged \
    hwdsl2/ipsec-vpn-server`;
  } catch {
    try {
      await $`docker restart ipsec-vpn-server`;
    } catch {
      // 
    }
  }
  
  await $`sleep 10`
  // await $`docker exec -it ipsec-vpn-server ikev2.sh --addclient vpnuser_1`
  // await $`docker exec -it ipsec-vpn-server ikev2.sh --exportclient vpnuser_1`
  // await $`docker exec -it ipsec-vpn-server ikev2.sh --addclient vpnuser_2`
  // await $`docker exec -it ipsec-vpn-server ikev2.sh --exportclient vpnuser_2`
  // await $`docker exec -it ipsec-vpn-server ikev2.sh --addclient vpnuser_3`
  // await $`docker exec -it ipsec-vpn-server ikev2.sh --exportclient vpnuser_3`
  // await $`docker exec -it ipsec-vpn-server ikev2.sh --addclient vpnuser_4`
  // await $`docker exec -it ipsec-vpn-server ikev2.sh --exportclient vpnuser_4`

  await db.set("DOCKER_IPSEC_INITIALIZED", true);

}


if (!(await db.get("DEFAULT_IPSEC_CREDENTIALS_EXPORTED"))) {
  try {
    await $`docker exec ipsec-vpn-server ikev2.sh --exportclient vpnuser`;
  } catch {}
  await $`docker cp ipsec-vpn-server:/etc/ipsec.d/vpnuser.p12 ./data/vpnuser.p12`;
  await $`docker cp ipsec-vpn-server:/etc/ipsec.d/vpnuser.mobileconfig ./data/vpnuser_unprocessed.mobileconfig`;
  await $`docker cp ipsec-vpn-server:/etc/ipsec.d/vpnuser.sswan ./data/vpnuser.sswan`;

  fs.writeFileSync(
    "./data/vpnuser.mobileconfig",
    fs
      .readFileSync(`./data/vpnuser_unprocessed.mobileconfig`)
      .toString()
      //  <key>PayloadOrganization</key>
      // <string>YourName, Inc.</string>
      .replace(/IKEv2 VPN/g, "Твой ВПН")
      // <key>UserDefinedName</key>
      // <string>xxx.xxx.xxx.xxx</string>
      .replace(
        /(<key>UserDefinedName<\/key>)(\s*<string>)(\d+\.\d+\.\d+\.\d+)(<\/string>)/,
        "$1$2Твой ВПН $3$4"
      )
  );

  await db.set("DEFAULT_IPSEC_CREDENTIALS_EXPORTED", true);

}

if (!(await db.get("DEFAULT_IPSEC_CREDENTIALS_UPLOADED"))) {
  const jsonData = {
    ip: await db.get("IP"),
    psk: await db.get("PSK"),
    hostname: await db.get("HOSTNAME"),
    login: "vpnuser",
    password: await db.get("DEFAULT_PASSWORD"),
  };

  await http.post(`/api/webhooks/server/upload-credentials`, {
    server_id: await db.get("HOSTNAME"),
    sswan: fs.readFileSync("./data/vpnuser.sswan").toString(),
    p12: fs.readFileSync("./data/vpnuser.p12").toString(),
    mobileconfig: fs.readFileSync("./data/vpnuser.mobileconfig").toString(),
    json: JSON.stringify(jsonData),
  });
  await db.set("DEFAULT_IPSEC_CREDENTIALS_UPLOADED", true);
}
